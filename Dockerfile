FROM node:12.8-buster-slim

RUN apt-get update && apt-get -y install \
    git wget \
    unoconv \
    && rm -rf /var/lib/apt/lists/*

RUN wget ftp://ftp.psu.ac.th/pub/thaifonts/sipa-fonts/*ttf -P /usr/share/fonts/truetype/thai

ENTRYPOINT /usr/bin/unoconv --listener --server=0.0.0.0 --port=2002
